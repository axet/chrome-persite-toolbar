# Chrome Per-Site Toolbar

Sometimes you want to filter content or do additional action on web site you visit. You create javascript, convert it to booklet and run on any page you want.

If you have many booklets, it will flood your toolbar and make it hard to manage and apply scripts on every website. As a solution you need dynamically
generated booklets toolbar per site you visit and automaticaly apply a scripts.

This extension will create side toolbar with booklets corresponding to parent folder site name
(I tried to use top toolbar instead, but some sites have scrolling issues, know bug).

# Flags

  * "(auto)" - adding this text to the bookmark name will automatically executed on tab load complete.

# Booklets

To create booklet you simple wrap your javascript with following code and add it as a bookmark url:

```javascript
javascript:(function(){
   // your javascript code here
})();
```

# Screenshots

  * Bookmarks Manager

![shot1](/docs/screenshot.png)

  * Web side toolbar

![shot2](/docs/screenshot1.png) 