const TOOLBAR_WIDTH = "130px";
const OPEN = 'open';
const AUTO = '(auto)';

function toolbar(bookmarks) {
  return `<!DOCTYPE html>
<html>
<head>
<style>
body {
  margin: 0;
}

.sidenav {
    width: ${TOOLBAR_WIDTH};
    position: fixed;
    z-index: 1;
    top: 20px;
    left: 10px;
    background: #eee;
    overflow-x: hidden;
    padding: 8px 0;
}

.sidenav a {
    padding: 6px 8px 6px 16px;
    text-decoration: none;
    font-size: 25px;
    color: #2196F3;
    display: block;
}

.sidenav a:hover {
    color: #064579;
}
</style>
</head>
<body>

<div class="sidenav">
  ${bookmarks.map(bookmark => `<a class="active" href="javascript:(function(){window.postMessage({type: '${OPEN}', text: '${bookmark.id}'}, '*');})()">${bookmark.title}</a>`).join('')}
</div>

</body>
</html>`;
}

function iframe(html) {
  return `javascript:(function(){var id = 'persitetoolbar';
if (document.getElementById(id)) { return; }
var iframe = document.createElement('iframe');
iframe.id = id;
iframe.srcDoc = '';
iframe.align = 'right';
iframe.onload = function() {
   var content = iframe.contentDocument || iframe.contentWindow.document;
   content.body.innerHTML = \`${html}\`;
}
iframe.style.height = '100%';
iframe.style.width = '${TOOLBAR_WIDTH}';
iframe.style.position = 'fixed';
iframe.style.top = '0';
iframe.style.right = '0';
iframe.style.borderWidth = '0px';
iframe.style.zIndex = '99999';
document.documentElement.appendChild(iframe);
iwindow = iframe.contentWindow || iframe;
iwindow.addEventListener("message", function(e) {
  chrome.runtime.sendMessage(e.data);
}, false);})()`
}

//
// https://github.com/miguelmota/is-valid-domain/blob/master/is-valid-domain.js
//
function isValidDomain(v) {
  if (typeof v !== 'string') return false

  var parts = v.split('.')
  if (parts.length <= 1) return false

  var tld = parts.pop()
  var tldRegex = /^[a-zA-Z0-9]+$/gi

  if (!tldRegex.test(tld)) return false

  var isValid = parts.every(function(host) {
    var hostRegex = /^(?!:\/\/)([a-zA-Z0-9]+|[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9])$/gi;
    return hostRegex.test(host)
  })

  return isValid
}

function getHost(href) {
  var a = document.createElement("a");
  a.href = href;
  return a.hostname;
};

function executeScript(tab, bb) { // process bookmarks folder
  var ff = [];
  for (var i in bb) {
    var b = bb[i];
    if (b.title.indexOf(AUTO) > 0) {
      console.log("'" + tab.title + "' executing '" + b.title + "'");
      openBookmark(tab, b); // auto bookmark
    } else {
      ff.push(b);
    }
  }
  if (ff.length == 0)
    return;
  console.log("'" + tab.title + "' create toolbar " + ff);
  var s = toolbar(ff);
  chrome.tabs.executeScript(tab.id, {
    code: iframe(s)
  });
}

function processBookmarks(tab, bb) { // oncomplete bookmarks parsing
  var h = getHost(tab.url);
  for (var i = 0; i < bb.length; i++) {
    var b = bb[i];
    if (isValidDomain(b.title) && b.children && h.indexOf(b.title) >= 0)
      executeScript(tab, b.children);
    if (b.children)
      processBookmarks(tab, b.children);
  }
}

function openBookmark(tab, b) {
  chrome.tabs.executeScript(tab.id, {
    code: b.url
  });
}

function openBookmarks(tab, id, bb) {
  for (var i = 0; i < bb.length; i++) {
    var b = bb[i];
    if (b.id == id) {
      console.log("open bookmark '" + b.title + "'");
      openBookmark(tab, b);
    }
    if (b.children)
      openBookmarks(tab, id, b.children);
  }
}

chrome.runtime.onMessage.addListener(
  function(req, sender, response) {
    if (req.type == OPEN) {
      chrome.bookmarks.getTree(function(bb) { openBookmarks(sender.tab, req.text, bb) } );
    }
  }
);

chrome.tabs.onUpdated.addListener(
  function(id, info, tab) {
    if (info.status == "complete")
      chrome.bookmarks.getTree(function(bb) { processBookmarks(tab, bb) } );
  }
);
